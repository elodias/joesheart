#!/bin/bash

YUI="$YUIPATH/yuicompressor-2.4.jar"
DIR=$(dirname $0)

cat "$DIR/lib/plugins/textureatlas.enchant.js" \
 	"$DIR/src/globals.js" \
	"$DIR/src/HeartBeat.js" \
	"$DIR/src/MusicPlayer.js" \
	"$DIR/src/ScrollableBackground.js" \
	"$DIR/src/scenes/StartScene.js" \
	"$DIR/src/scenes/WalkScene.js" \
	"$DIR/src/scenes/DogScene.js" \
	"$DIR/src/scenes/DiscoScene.js" \
	"$DIR/src/scenes/WeddingScene.js" \
	"$DIR/src/scenes/OfficeScene.js" \
	"$DIR/src/scenes/HospitalScene.js" \
	"$DIR/src/scenes/GameOverScene.js" \
	"$DIR/src/scenes/CreditsScene.js" \
	"$DIR/src/scenes/LoadScene.js" \
	"$DIR/src/main.js" > "$DIR/combined.js"

java -jar $YUI --type js --charset utf-8 -o "$DIR/joesheart.min.js" "$DIR/combined.js"
rm "$DIR/combined.js"