var ScrollableBackground = enchant.Class.create(enchant.Group, {
    _sprites: null,
    _wi: null,
    _totalWi: null,
    _scroll: null,
    _idx: null,
    _accum: null,
    _alternate: null,
    initialize: function(width, generatorFunc, alternate){
        Group.call(this);
        this._scroll = { x: 0, y: 0};
        this._totalWi = width;
        this._sprites = [];
        this._idx = 0;
        this._accum = 0;
        this._alternate = typeof alternate == "undefined" ? true : alternate;
        var wi = 0;
        var i = 0;
        do {
            var s = generatorFunc.call(this, i);
            s.x = wi;
            wi += s.width;
            this._wi = s.width;
            this.addChild(s);
            this._sprites.push(s);
            i++;
        } while(wi < width + this._wi);
        this._totalWi = wi;
    },
    scrollTo: function(xp, yp){
        var xpm = xp % (this._totalWi - this._wi);
        if(xpm > 0){
            xpm -= this._wi;
        }
        
        if(this._alternate){
            this._accum += xp - this._scroll.x;
            while(this._accum >= this._wi){
                this._accum -= this._wi;
                this._idx++;
            }
            
            while(this._accum <= -this._wi){
                this._accum += this._wi;
                this._idx--;
            }
            if(this._idx < 0){
                this._idx = this._sprites.length + this._idx;
            }
        }
        
        for(var i = 0, len = this._sprites.length; i < len; i++){
            this._sprites[(i + this._idx) % len].x = xpm + i * this._wi;
        }
        
        this._scroll.x = xp;
        this._scroll.y = yp;
        
        
    },
    
    scrollBy: function(xp, yp){
        this.scrollTo(this._scroll.x + xp, this._scroll.y + yp);
    },
    
    getScrollX: function(){
        return this._scroll.x;
    },
    getScrollY: function(){
        return this._scroll.y;
    }
});