var HeartBeat = enchant.Class.create({
   _lastBeat: 0,
   _values: [],
   _idx: 0,
   _samples: 3,
   _interval: 0,
   _timeout: null,
   _listener: null,
   _lastAverage:0,
   _currAverage:0,
   _sounds: null,
   _sndIdx: 0,
   _owner: null,
   _callback: null,
   _heart1: null,
   _heart2: null,
   
   /**
    * Heartbeat constructor
    * @param interval the interval at which the heart should beat in seconds
    * @param owner the owner that fires the touch/button events
    * @param numSamples number of samples to average over
    * @param callback function that will receive all beat values
    */
   initialize: function(interval, owner, numSamples, callback){
       var self = this;
       
       this._interval = interval;
       this._samples = numSamples || 3;
       this._lastBeat = Core.instance.getElapsedTime();
       this._resetTimeout(this);
       this._lastAverage = 0;
       this._currAverage = 0;
       this._owner = owner;
       this._callback  = callback;
       
       this._heart1 = new enchant.AtlasSprite("Atlas", "s1-title-heart-small.png");
       this._heart2 = new enchant.AtlasSprite("Atlas", "s1-title-heart.png");
       this._heart1.y = this._heart2.y = 2;
       this._heart1.x = this._heart2.x = Core.instance.width - this._heart1.width - 2;
       this._owner.addChild(this._heart1);
       this._owner.addChild(this._heart2);
       this._heart2.visible = false;
       
       var sound = Core.instance.assets["assets/sound/heart" + (joesheart.useOgg ? ".ogg" : ".mp3")];
       if(sound){
           this._sounds = [];
           this._sndIdx = 0;
           for(var i = 0; i < 10; i++){
               this._sounds.push(sound.clone());
           }
       }
       
       this._values = [];
       for(var i = 0; i < this._samples; i++){
           this._values[i] = 0;
       }
       
       this._listener = function(){
           self._heart2.visible = true;
           self._heart2.tl.show().delay(2).hide();
           self.beat(true);
       };
       
       this._owner.addEventListener(
               enchant.ENV.TOUCH_ENABLED ? enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, 
               this._listener);
   },
   
   /**
    * Trigger a heart-beat.
    * Will call the "callback" function with the average (-1 = too slow, 0 = optimal, 1 = too fast)
    */
   beat: function(playSound){
       this._resetTimeout(this);
       
       var game = Core.instance;
       var t = game.getElapsedTime();
       var delta = t -  this._lastBeat;
       this._lastBeat = t;
       
       var amp = Math.sin(Math.PI * 0.5 + Math.PI * 0.5 / this._interval * delta);
       this._pushValue(amp);
       
       if(playSound && this._sounds){
          try {
            this._sounds[this._sndIdx].stop();
          } catch(err){
          }

          this._sndIdx = (this._sndIdx + 1) % this._sounds.length;
          this._sounds[this._sndIdx].play();
       }
   },
   
   /**
    * Get the influence of the heart-beat. This is an interpolated
    * Number between the last and the current heartbeat.
    * @return the strength/influence of the heart (-1 = too slow, 0 = optimal, 1 = too fast)
    */
   influence: {
       get: function(){
           var t = Core.instance.getElapsedTime();
           var delta = Math.min(t - this._lastBeat, this._interval);
           return this._lastAverage + (this._currAverage - this._lastAverage) / this._interval * delta;
       }
   },
   
   /**
    * The heart-beat interval
    */
   interval: {
       get: function() {
           return this._interval;
       },
       set: function(interval) {
           this._interval = interval;
       }
   },
   
   /**
    * Clear event listeners
    */
   dispose: function(){
       clearInterval(this._timeout);
       this._owner.removeEventListener(
               enchant.ENV.TOUCH_ENABLED ? enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, 
               this._listener);
       this._owner.removeChild(this._heart1);
       this._owner.removeChild(this._heart2);
       this._callback = null;
       this._sounds = null;
       this._owner = null;
       this._heart1 = null;
       this._heart2 = null;
   },
   
   _pushValue: function(value){
       this._values[this._idx] = value;
       this._idx = (this._idx + 1) % this._samples;
       
       var avg = 0;
       var len = this._values.length;
       for(var i = 0; i < len; ++i){
           avg += this._values[i];
       }
       avg/=len;
       avg = Math.max(-1, Math.min(1, avg));
       
       if(typeof this._callback == "function"){
           this._callback.call(this, avg);
       }
       
       this._lastAverage = this._currAverage;
       this._currAverage = avg; 
       return avg;
   },
   
   _resetTimeout: function(self){
       clearInterval(self._timeout);
       self._timeout = setInterval(function(){
           self._pushValue(-1);
       }, self._interval * 2000);
   }
});