var LoadScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        var game = Core.instance;
        this.backgroundColor = "#000000";
        
        var bg = new enchant.Sprite(140, 10);
        bg.x = 10;
        bg.y = 40;
        bg.backgroundColor = "#808080";
        this.addChild(bg);
        
        var bar = new enchant.Sprite(140, 10);
        bar.x = 10;
        bar.y = 40;
        bar.backgroundColor = "#ff0000";
        bar.width = 1;
        this.addChild(bar);
        var self = this;
        var progressListener = function(e){
            bar.width = Math.round(bg.width / e.total * e.loaded);
        };

        var loadListener = function(e){
            self.removeEventListener(Event.PROGRESS, progressListener);
            self.removeEventListener(Event.LOAD, loadListener);

            var core = enchant.Core.instance;
            core.removeScene(core.loadingScene);
            core.dispatchEvent(e);
        };

        self.addEventListener(Event.LOAD, loadListener);
        self.addEventListener(Event.PROGRESS, progressListener);
    }
});