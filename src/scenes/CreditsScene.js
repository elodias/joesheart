var CreditsScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        
        this.backgroundColor = "black";
        
        var bg = new enchant.AtlasSprite("Atlas", "credits.png");
        var scrollHeight = -bg.height + 90;
        
        var group = new enchant.Group();
        this.addChild(group);
        group.addChild(bg);
        
        var sx = 65;
        var sy = 144;
        
        var year = new Date().getFullYear();
        var endYear = year + joesheart.age;
        var str = year + "-" + endYear;
        for(var i = 0, len = str.length; i < len; i++){
            var chr = str.charAt(i);
            var s = new enchant.AtlasSprite("Atlas", "n"+chr+".png");
            s.x = sx + i * 4;
            s.y = sy;
            group.addChild(s);
        }
        
        var music = new MusicPlayer().play("assets/sound/credits", true);
        
        game.fps = 15;
        var frameListener = function(){
            if(group.y > scrollHeight){
                group.y -= 1;
            }
        };
        
        var self = this;
        
        var buttonListener = function(){
            self.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
            self.removeEventListener(
                    enchant.ENV.TOUCH_ENABLED ? enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, 
                    buttonListener);
            music.stop();
            joesheart.trackScene("Credits", "End");
            game.replaceScene(new StartScene());
        };

        self.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
        joesheart.trackScene("Credits", "Start");
        // delay slightly so that user doesn't quit immediately
        this.tl.delay(15).then(function(){
            self.addEventListener(
                    enchant.ENV.TOUCH_ENABLED ? enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, 
                    buttonListener);
        });
    }
});