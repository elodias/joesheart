var OfficeScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);

        var game = Core.instance;

        joesheart.age = 48;

        var bg = new enchant.AtlasSprite("Atlas", "s6-backdrop.png");
        this.addChild(bg);

        var plant = new enchant.AtlasSprite("Atlas", "s6-plant.png");
        plant.x = 107;
        plant.y = 30;
        this.addChild(plant);

        var clock = new enchant.AtlasSprite("Atlas", "s6-clock-0.png");
        clock.x += 38;
        clock.y += 8;
        this.addChild(clock);

        var screen = new enchant.AtlasSprite("Atlas", "s6-screen-0.png");
        screen.x += 71;
        screen.y += 25;
        this.addChild(screen);
        
        var workStack = [];
        
        for(var i = 0; i < 12; i++){
            var paper = new enchant.AtlasSprite("Atlas", "s6-paper.png");
            paper.x = 49;
            paper.y = 47 - i * 2;
            this.addChild(paper);
            workStack.push(paper);
        }
        
        var workIndex = workStack.length - 1;

        var avatar = new enchant.AtlasSprite("Atlas", "s6-work-0.png");
        avatar.y = 35;
        avatar.x = 70;
        this.addChild(avatar);

        var ghost = new enchant.AtlasSprite("Atlas", "ghost.png");
        ghost.visible = false;
        this.addChild(ghost);

        var frame = 0;
        var cooldown = 0;
        var clockFrame = 0;
        var screenFrame = 0;

        var STATE_IDLE = 0,
            STATE_WORK = 1,
            STATE_WORK_CRAZY = 2,
            STATE_DIE = 3;

        var state = STATE_IDLE;

        var hb = new HeartBeat(0.75, this);

        game.fps = 5;
        
        var music = new MusicPlayer().play("assets/sound/office", true);
        music.asset.volume = 0.6;
        joesheart.trackScene("Office", "Start");
        var lastTime = game.getElapsedTime();
        var frameListener = function(){
            var t = game.getElapsedTime();
            var delta = (t - lastTime);
            lastTime = t;
            
            var inf = hb.influence;
            
            clock.spriteId = "s6-clock-" + (clockFrame++ % 6) + ".png";
            
            if(inf < -0.999){
                state = STATE_DIE;
            }
            
            if(state == STATE_IDLE || state == STATE_WORK){
                if(cooldown > 0){
                    cooldown -= delta;
                } else {
                    cooldown = 1;
                    workStack[workIndex].visible = true;
                    workIndex++;
                    hb.interval = 0.3 + 0.45 / workStack.length * workIndex;
                    if(workIndex >= workStack.length){
                        workIndex = workStack.length - 1;
                    }
                }
            }

            switch(state){
                case STATE_IDLE:
                    if(inf > 0.0){
                        state = STATE_WORK;
                        avatar.spriteId = "s6-work-0.png";
                    }
                    break;
                case STATE_WORK:
                    avatar.spriteId = "s6-work-" + (frame++ % 2) + ".png";
                    if(inf > 0.6){
                        state = STATE_WORK_CRAZY;
                    } else if(inf < -0.4){
                        state = STATE_IDLE;
                        avatar.spriteId = "s6-work-1.png";
                    }
                    break;
                case STATE_WORK_CRAZY:
                    screen.spriteId = "s6-screen-" + (screenFrame++ % 6) + ".png";
                    avatar.spriteId = "s6-fast-" + (frame++ % 2) + ".png";
                    if(inf < 0.6){
                        state = STATE_WORK;
                    }
                    
                    if(cooldown > 0){
                        cooldown -= delta;
                    } else {
                        cooldown = 1;
                        workStack[workIndex].visible = false;
                        hb.interval = 0.3 + 0.45 / workStack.length * workIndex;
                        workIndex--;
                        if(workIndex < 0){
                            avatar.spriteId = "s6-dead.png";
                            this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                            hb.dispose();
                            avatar.x -= 20;
                            avatar.y += 20;
                            avatar.tl.delay(20).then(function(){
                                music.stop();
                                joesheart.trackScene("Office", "Win");
                                game.replaceScene(new HospitalScene());
                            });
                        }
                    }
                    
                    break;
                case STATE_DIE:
                    avatar.spriteId = "s6-dead.png";
                    avatar.x -= 20;
                    avatar.y += 20;
                    this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                    ghost.x = avatar.x + avatar.width - 10;
                    ghost.y = avatar.y;
                    ghost.visible = true;
                    hb.dispose();
                    game.fps = 60;
                    ghost.tl.moveBy(20, -70, 90, enchant.Easing.CUBIC_EASEIN).then(function(){
                        music.stop();
                        joesheart.trackScene("Office", "Fail");
                        game.replaceScene(new GameOverScene());
                    });
                    break;
            }
        };
        
        this.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
    }
});