var DiscoScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        var game = Core.instance;
        game.fps = 5;
        joesheart.age = 20;
        
        var bg = new enchant.AtlasSprite("Atlas", "s4-bg-back.png");
        this.addChild(bg);
        
        for(var i = 0; i < 4; i++){
            var light = new enchant.AtlasSprite("Atlas", "s4-lights-"+i+".png");
            this.addChild(light);
            light.tl.hide().delay((i + 1) * 4).show().delay(Math.random() * 3 + 5).loop();
        }
        
        var bg2 = new enchant.AtlasSprite("Atlas", "s4-bg-front.png");
        this.addChild(bg2);
        
        var avatar = new enchant.AtlasSprite("Atlas", "s4-joe-idle.png");
        avatar.y = 27;
        avatar.x = 38;
        this.addChild(avatar);
        
        var girl = new enchant.AtlasSprite("Atlas", "s4-girl-0.png");
        girl.y = 38;
        girl.x = 160;
        this.addChild(girl);
        
        var heart = new enchant.AtlasSprite("Atlas", "s4-love.png");
        heart.x = avatar.x + 30;
        heart.y = avatar.y - 2;
        heart.visible = false;
        this.addChild(heart);
        
        var ghost = new enchant.AtlasSprite("Atlas", "ghost.png");
        ghost.visible = false;
        this.addChild(ghost);
        
        var STATE_IDLE = 0,
            STATE_SLOW = 1,
            STATE_DANCE = 2,
            STATE_TURBO = 3,
            STATE_KISS = 4,
            STATE_DIE = 5;
        
        var state = STATE_IDLE;
        
        var hb = new HeartBeat(0.6, this);
        
        var frame = 0;
        var girlFrame = 0;
        var frameskip = 0;
        var girlFar = game.width - 20;
        var girlClose = avatar.x + 20;
        
        var music = new MusicPlayer().play("assets/sound/disco", true);
        music.asset.volume = 0.6;
        joesheart.trackScene("Disco", "Start");
        
        var frameListener = function(){
            var inf = hb.influence;
            girl.spriteId = "s4-girl-" + (girlFrame++ % 3) + ".png";
            
            if(Math.abs(inf) < 0.2 && state == STATE_DANCE){
                girl.x--;
            } else {
                girl.x++;
            }
            if(girl.x < girlClose){
                state = STATE_KISS;
            }
            girl.x = Math.max(girlClose, Math.min(girlFar, girl.x));
            
            if(frameskip > 0){
                frameskip--;
                return;
            }
            
            if(inf < -0.999){
                state = STATE_DIE;
            }
            
            switch(state){
            case STATE_IDLE:
                if(inf > 0.0){
                    frameskip = 3;
                    state = STATE_DANCE;
                } else if(inf < -0.3){
                    state = STATE_SLOW;
                    frameskip = 3;
                    avatar.spriteId = "s4-joe-slow.png";
                }
                break;
            case STATE_SLOW:
                if(inf > -0.1){
                    state = STATE_IDLE;
                    frameskip = 3;
                    avatar.spriteId = "s4-joe-idle.png";
                }
                break;
            case STATE_DANCE:
                if(inf > 0.4){
                    state = STATE_TURBO;
                    avatar.spriteId = "s4-joe-fast-start.png";
                    frameskip = 3;
                } else if(inf < -0.4){
                    state = STATE_IDLE;
                    avatar.spriteId = "s4-joe-idle.png";
                } else {
                    avatar.spriteId = "s4-joe-dance-" + (frame++ % 5) + ".png";
                }
                break;
            case STATE_TURBO:
                if(inf < 0.3){
                    state = STATE_DANCE;
                } else {
                    avatar.spriteId = "s4-joe-fast-" + (frame++ % 3) + ".png";
                }
                break;
            case STATE_KISS:
                avatar.spriteId = "s4-joe-kiss.png";
                this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                girl.visible = false;
                hb.dispose();
                heart.visible = true;
                heart.tl.show().delay(3).hide().delay(3).show().delay(3).hide().delay(3).show().delay(3).hide().delay(3).then(function(){
                    music.stop();
                    joesheart.trackScene("Disco", "Win");
                    game.replaceScene(new WeddingScene());
                });
                break;
            case STATE_DIE:
                avatar.spriteId = "s4-joe-dead.png";
                this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                ghost.x = avatar.x + 36;
                ghost.y = avatar.y + 32;
                ghost.visible = true;
                hb.dispose();
                game.fps = 60;
                ghost.tl.moveBy(20, -70, 90, enchant.Easing.CUBIC_EASEIN).then(function(){
                    music.stop();
                    joesheart.trackScene("Disco", "Fail");
                    game.replaceScene(new GameOverScene());
                });
                break;
            }
        };

        this.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
    }
});