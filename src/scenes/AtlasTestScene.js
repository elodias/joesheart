joesheart.assets.push("assets/atlas/AtlasTest.png","assets/atlas/AtlasTest.json");

var AtlasTestScene = enchant.Class.create(enchant.Scene, {
    
    initialize: function(){
        Scene.call(this);
        var game = Core.instance;
        
        enchant.TextureAtlas.createTextureAtlas("AtlasTest",
                game.assets["assets/atlas/AtlasTest.json"],
                game.assets["assets/atlas/AtlasTest.png"]);
        
        this.backgroundColor = "white";
        var counter = 1, totalFrames = 120;
        var sprite = new enchant.AtlasSprite("AtlasTest", "time000" + counter +".png");
        this.addChild(sprite);
        Core.instance.addEventListener("enterframe", function(){
           counter = (counter + 1) % totalFrames +1;
           sprite.spriteId = "time0" + (counter < 100 ? counter < 10 ? "00" + counter : "0" + counter : counter) + ".png";
        });
        
    }    
});