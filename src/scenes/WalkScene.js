var WalkScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        
        joesheart.age = 2;
        game.fps = 6;
        
        var bg = new ScrollableBackground(game.width, function(){
            return new enchant.AtlasSprite("Atlas", "s2-backdrop.png");
        });
        this.addChild(bg);
        
        var avatar = new enchant.AtlasSprite("Atlas", "s2-stand-0.png");
        avatar.y = 37;
        avatar.x = 40;
        this.addChild(avatar);
        
        var ghost = new enchant.AtlasSprite("Atlas", "ghost.png");
        ghost.visible = false;
        this.addChild(ghost);
        
        var cooldown = 0;
        var frame = 0;
        var walkGoal = 8;
        var walked = 0;
        var frameSkip = 0;
        
        var STATE_IDLE = 0,
            STATE_STANDUP = 1,
            STATE_WALK = 2,
            STATE_FALL = 3,
            STATE_CRY = 4,
            STATE_DIE = 5;
        
        var state = STATE_IDLE;
        
        var hb = new HeartBeat(0.75, this);
        
        var music = new MusicPlayer().play("assets/sound/walking", true);
        music.asset.volume = 0.6;
        joesheart.trackScene("Walk", "Start");
        var lastTime = game.getElapsedTime();
        var frameListener = function(){
            var t = game.getElapsedTime();
            var delta = (t - lastTime);
            lastTime = t;
            cooldown -= delta;

            var inf = hb.influence;
            
            if(state == STATE_WALK){
                walked += delta;
                if(walked > walkGoal){
                    this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                    hb.dispose();
                    music.stop();
                    joesheart.trackScene("Walk", "Win");
                    game.replaceScene(new DogScene());
                }
            }
            
            if(inf < -0.9){
                state = STATE_DIE;
            }
            
            if(frameSkip > 0){
                frameSkip--;
                return;
            }
            
            switch(state){
            case STATE_IDLE:
                if(inf > 0.1){
                    state = STATE_STANDUP;
                    avatar.spriteId = "s2-stand-1.png";
                }
                break;
            case STATE_STANDUP:
                if(inf > 0.2){
                    state = STATE_WALK;
                    avatar.spriteId = "s2-walk-0.png";
                } else if(inf < -0.1){
                    state = STATE_IDLE;
                    avatar.spriteId = "s2-stand-0.png";
                }
                break;
            case STATE_WALK:
                if(inf > 0.6) {
                    state = STATE_FALL;
                    avatar.spriteId = "s2-fall-0.png";
                    walked = 0;
                } else if(inf < -0.3){
                    state = STATE_STANDUP;
                    avatar.spriteId = "s2-stand-1.png";
                    walked = 0;
                } else {
                    bg.scrollBy(-3,0);
                    frameSkip = 1;
                    avatar.spriteId = "s2-walk-"+ (frame++ % 3) +".png";
                }
                break;
            case STATE_FALL:
                avatar.spriteId = "s2-fall-1.png";
                state = STATE_CRY;
                break;
            case STATE_CRY:
                if(inf < 0.35){
                    state = STATE_IDLE;
                    avatar.spriteId = "s2-stand-0.png";
                }
                break;
            case STATE_DIE:
                avatar.spriteId = "s2-dead.png";
                this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                ghost.x = avatar.x + 34;
                ghost.y = avatar.y + 12;
                ghost.visible = true;
                hb.dispose();
                game.fps = 60;
                ghost.tl.moveBy(20, -70, 90, enchant.Easing.CUBIC_EASEIN).then(function(){
                    music.stop();
                    joesheart.trackScene("Walk", "Fail");
                    game.replaceScene(new GameOverScene());
                });
                break;
            }
        };
        
        this.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
    }
});