var WeddingScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        game.fps = 5;
        joesheart.age = 30;
        
        var bg = new enchant.AtlasSprite("Atlas", "s5-backdrop.png");
        this.addChild(bg);
        
        
        var avatar = new enchant.AtlasSprite("Atlas", "s5-try-0.png");
        avatar.x = 39;
        avatar.y = 27;
        this.addChild(avatar);
        
        var ghost = new enchant.AtlasSprite("Atlas", "ghost.png");
        ghost.visible = false;
        this.addChild(ghost);
        
        var STATE_TRY = 0,
            STATE_SLOW = 1,
            STATE_FAST = 2,
            STATE_SUCCESS = 4,
            STATE_DIE = 5;
        
        var state = STATE_TRY;
        
        var trying = 0;
        var frame = 0;
        var frameskip = 0;
        
        var hb = new HeartBeat(1.1, this);
        
        var music = new MusicPlayer().play("assets/sound/wedding", true);
        music.asset.volume = 0.6;
        joesheart.trackScene("Wedding", "Start");
        var lastTime = game.getElapsedTime();
        var frameListener = function(){
            var t = game.getElapsedTime();
            var delta = (t - lastTime);
            lastTime = t;
            
            trying += delta;
            var inf = hb.influence;
            
            if(frameskip > 0){
                frameskip--;
                return;
            }
            
            if(inf < -0.999){
                state = STATE_DIE;
            }
            
            if(trying > 5){
                state = STATE_SUCCESS;
            }
            
            switch(state){
            case STATE_TRY:
                avatar.spriteId = "s5-try-" + (frame++ % 3) + ".png";
                if(inf < -0.3){
                    avatar.spriteId = "s5-slow.png";
                    frameskip = 3;
                    state = STATE_SLOW;
                } else if(inf > 0.3){
                    state = STATE_FAST;
                }
                break;
            case STATE_SLOW:
                trying = 0;
                if(inf > 0.0){
                    state = STATE_TRY;
                }
                break;
            case STATE_FAST:
                trying = 0;
                avatar.spriteId = "s5-fast-" + (frame++ % 3) + ".png";
                if(inf < 0.2){
                    state = STATE_TRY;
                }
                break;
            case STATE_SUCCESS:
                avatar.spriteId = "s5-success-0.png";
                this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                avatar.tl.delay(8).then(function(){
                    avatar.spriteId = "s5-success-1.png";
                }).delay(8).then(function(){
                    hb.dispose();
                    music.stop();
                    joesheart.trackScene("Wedding", "Win");
                    game.replaceScene(new OfficeScene());
                });
                break;
            case STATE_DIE:
                trying = 0;
                avatar.spriteId = "s5-dead.png";
                this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                ghost.x = avatar.x;
                ghost.y = avatar.y + 63;
                ghost.visible = true;
                hb.dispose();
                game.fps = 60;
                ghost.tl.moveBy(0, -120, 90, enchant.Easing.CUBIC_EASEIN).then(function(){
                    music.stop();
                    joesheart.trackScene("Wedding", "Fail");
                    game.replaceScene(new GameOverScene());
                });
                break;
            }
        };
        
        this.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
    }
});