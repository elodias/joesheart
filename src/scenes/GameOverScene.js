var GameOverScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        
        this.backgroundColor = "purple";
        
        var clouds = new ScrollableBackground(game.width, function(i){
            return new enchant.AtlasSprite("Atlas", "s8-clouds-"+ i +".png");
        });
        this.addChild(clouds);
        
        var bg = new enchant.AtlasSprite("Atlas", "s8-bg.png");
        this.addChild(bg);
        
        var sx = 65;
        var sy = 49;
        
        var year = new Date().getFullYear();
        var endYear = year + joesheart.age;
        var str = year + "-" + endYear;
        for(var i = 0, len = str.length; i < len; i++){
            var chr = str.charAt(i);
            var s = new enchant.AtlasSprite("Atlas", "n"+chr+".png");
            s.x = sx + i * 4;
            s.y = sy;
            this.addChild(s);
        }
        
        var music = new MusicPlayer().play("assets/sound/game-over");
        var self = this;
        
        game.fps = 7;
        
        var frameListener = function(){
            clouds.scrollBy(-1,0);
        };
        joesheart.trackScene("GameOver", "Start");
        var buttonListener = function(){
            self.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
            self.removeEventListener(
                    enchant.ENV.TOUCH_ENABLED ? enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, 
                    buttonListener);
            music.stop();
            joesheart.trackScene("GameOver", "End");
            game.replaceScene(new StartScene());
        }

        self.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
        
        this.tl.delay(15).then(function(){
            self.addEventListener(
                    enchant.ENV.TOUCH_ENABLED ? enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, 
                    buttonListener);
        });
    }
});