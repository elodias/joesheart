var DogScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        
        joesheart.age = 14;
        
        var bg = new ScrollableBackground(game.width * 2, function(i){
            return new enchant.AtlasSprite("Atlas", "s3-bg-part-"+ i +".png");
        }, false);
        this.addChild(bg);
        
        var dawg = new enchant.AtlasSprite("Atlas", "s3-dawg-idle-left.png");
        dawg.y = 38;
        dawg.x = 50;
        this.addChild(dawg);
        
        var avatar = new enchant.AtlasSprite("Atlas", "s3-joe-idle.png");
        avatar.y = 38;
        this.addChild(avatar);
        
        var ghost = new enchant.AtlasSprite("Atlas", "ghost.png");
        ghost.visible = false;
        this.addChild(ghost);
        
        var frame = 0;
        var dawgframe = 0;
        var speed = 2;
        var dogDist = dawg.x - avatar.x;
        
        var STATE_IDLE = 0,
            STATE_WALK = 1,
            STATE_STRUGGLE = 3,
            STATE_DIE = 4;
        
        var state = STATE_IDLE;
        
        var DOGSTATE_LEFT = 0,
            DOGSTATE_RIGHT = 1,
            DOGSTATE_CHASE = 2,
            DOGSTATE_CATCH = 3;
        
        var dogstate = DOGSTATE_LEFT;
        
        var hb = new HeartBeat(0.8, this);
        
        var music = new MusicPlayer().play("assets/sound/dog", true);
        music.asset.volume = 0.4;
        
        game.fps = 5;
        joesheart.trackScene("Dog", "Start");
        var frameListener = function(){
            var inf = hb.influence;
           
            var atEdge = game.width - avatar.x - 50 > 0 || bg.getScrollX() <= game.width * -1;
            
            if(inf < -0.999){
                state = STATE_DIE;
            }
            
            switch(state){
            case STATE_IDLE:
                if(inf > 0.0){
                    state = STATE_WALK;
                }
                break;
            case STATE_WALK:
                speed = 2 + Math.round(inf * 2);
                if(dogstate < DOGSTATE_CHASE){
                    hb.interval = (0.4 + 0.2 / dogDist * Math.max(0, dawg.x - avatar.x));
                }
                if(atEdge){
                    avatar.x += speed;
                    if(avatar.x >= game.width){
                        this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                        hb.dispose();
                        music.stop();
                        joesheart.trackScene("Dog", "Win");
                        game.replaceScene(new DiscoScene());
                    }
                } else {
                    bg.scrollBy(-speed, 0);
                }
                
                if(speed == 0){
                    state = STATE_IDLE;
                    avatar.spriteId = "s3-joe-idle.png";
                } else if(inf > 0.5 && dogstate == DOGSTATE_CHASE){
                    avatar.spriteId = "s3-joe-run-"+ (frame++ % 2) +".png";
                } else {
                    avatar.spriteId = "s3-joe-walk-"+ (frame++ % 2) +".png";
                }
                break;
            case STATE_STRUGGLE:
                break;
            case STATE_DIE:
                if(dogstate == DOGSTATE_CATCH){
                    dawg.spriteId = "s3-dawg-bite.png";
                    avatar.visible = false;
                } else {
                    avatar.spriteId = "s3-joe-dead-early.png";
                }
                
                this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                ghost.x = avatar.x + 26;
                ghost.y = avatar.y + 12;
                ghost.visible = true;
                hb.dispose();
                game.fps = 60;
                ghost.tl.moveBy(20, -70, 90, enchant.Easing.CUBIC_EASEIN).then(function(){
                    joesheart.trackScene("Dog", "Fail");
                    music.stop();
                    game.replaceScene(new GameOverScene());
                });
                break;
            }
            
            switch(dogstate){
            case DOGSTATE_LEFT:
                if(avatar.x > dawg.x + 5){
                    dawg.spriteId = "s3-dawg-idle-right.png";
                    dogstate = DOGSTATE_RIGHT;
                }
                break;
            case DOGSTATE_RIGHT:
                if(avatar.x > dawg.x + 40){
                    dogstate = DOGSTATE_CHASE;
                }
                break;
                
            case DOGSTATE_CHASE:
                if(atEdge){
                    dawg.x += 3;
                } else {
                    dawg.x += 3 - speed;
                }
                dawg.spriteId = "s3-dawg-run-"+ (dawgframe++ % 2) +".png";
                if(dawg.x > avatar.x - avatar.width){
                    dogstate = DOGSTATE_CATCH;
                    state = STATE_DIE;
                }
                break;
            case DOGSTATE_CATCH:
                break;
            }
        };
        
        this.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
    }
});