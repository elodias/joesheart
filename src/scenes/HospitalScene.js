var HospitalScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        joesheart.age = 70;
        
        this.backgroundColor = "blue";
        
        var clouds = new ScrollableBackground(game.width, function(i){
            return new enchant.AtlasSprite("Atlas", "s7-clouds.png");
        });
        this.addChild(clouds);
        
        var bg = new enchant.AtlasSprite("Atlas", "s7-wall.png");
        this.addChild(bg);
        
        var screen = new enchant.AtlasSprite("Atlas", "s7-screen-0.png");
        screen.x = 2;
        screen.y = 25;
        this.addChild(screen);
        
        var wife = new enchant.AtlasSprite("Atlas", "s7-walk-0.png");
        wife.x = 40;
        wife.y = 30;
        wife.visible = false;
        this.addChild(wife);
        
        var avatar = new enchant.AtlasSprite("Atlas", "s7-breathe-0.png");
        avatar.x = 31;
        avatar.y = 29;
        this.addChild(avatar);
        
        var bed = new enchant.AtlasSprite("Atlas", "s7-bed.png");
        this.addChild(bed);
        
        var ghost = new enchant.AtlasSprite("Atlas", "ghost.png");
        ghost.x = avatar.x + 14;
        ghost.y = avatar.y;
        ghost.visible = false;
        this.addChild(ghost);
        
        game.fps = 5;
        var frame = 0;
        var screenFrame = 0;
        var skip = 3;
        var frameSkip = 0;
        
        var STATE_IDLE = 0,
            STATE_DYEING = 1,
            STATE_DEAD = 2,
            STATE_WALK = 3;
        
        var state = STATE_IDLE;
        
        var hb = new HeartBeat(1, this);
        
        var music = new MusicPlayer().play("assets/sound/hospital", true);
        music.asset.volume = 0.5;
        joesheart.trackScene("Hospital", "Start");
        var frameListener = function(){
            var inf = hb.influence;
            
            clouds.scrollBy(-1,0);
            if(skip <= 0 && state == STATE_IDLE){
                screen.spriteId = "s7-screen-" + (++screenFrame) % 4 + ".png";
                skip = 4;
            }
            skip--;
            
            if(frameSkip > 0){
                frameSkip--;
                return;
            }
            
            switch(state){
            case STATE_IDLE:
                if(inf < -0.999){
                    state = STATE_DYEING;
                }
                avatar.spriteId = "s7-breathe-" + (++frame) % 2 + ".png";
                frameSkip = 3;
                break;
            case STATE_DYEING:
                screen.spriteId = "s7-screen-over.png";
                avatar.spriteId = "s7-dead-0.png";
                state = STATE_DEAD;
                frameSkip = 10;
                hb.dispose();
                ghost.visible = true;
                ghost.tl.moveBy(20, -70, 10, enchant.Easing.CUBIC_EASEIN);
                
                break;
            case STATE_DEAD:
                avatar.spriteId = "s7-dead-1.png";
                frameSkip = 30;
                state = STATE_WALK;
                wife.visible = true;
                break;
            case STATE_WALK:
                avatar.spriteId = "s7-dead-3.png";
                wife.spriteId = "s7-walk-" + (++frame) % 3 + ".png";
                wife.x += 2;
                if(wife.x > game.width){
                    this.removeEventListener(enchant.Event.ENTER_FRAME, frameListener);
                    music.stop();
                    joesheart.trackScene("Hospital", "Win");
                    game.replaceScene(new CreditsScene());
                }
                break;
            }
        };
        
        this.addEventListener(enchant.Event.ENTER_FRAME, frameListener);
    }
});