var StartScene = enchant.Class.create(enchant.Scene, {
    initialize: function(){
        Scene.call(this);
        
        var game = Core.instance;
        game.fps = 10;
        
        var bg = new enchant.AtlasSprite("Atlas", "s1-backdrop.png");
        this.addChild(bg);
        
        var title = new enchant.AtlasSprite("Atlas", "s1-title.png");
        title.x = 75;
        title.y = 18;
        this.addChild(title);
        
        var heart = new enchant.AtlasSprite("Atlas", "s1-title-heart.png");
        heart.x = 87;
        heart.y = 19;
        this.addChild(heart);
        heart.tl.hide().delay(6).show().delay(6).loop();
        
        var kbd2 = new enchant.AtlasSprite("Atlas", "s1-keyboard-arrow.png");
        kbd2.x = 79;
        kbd2.y = 35;
        kbd2.tl.hide().delay(6).show().delay(6).loop();
        kbd2.visible = false;
        
        var kbd = new enchant.AtlasSprite("Atlas", "s1-keyboard.png");
        kbd.x = 79;
        kbd.y = 35;
        this.addChild(kbd);
        this.addChild(kbd2);
        
        kbd.tl.hide().delay(30).show().then(function(){
            heart.visible = false;
            title.visible = false;
            kbd2.visible = true;
        }).delay(30).then(function(){
            heart.visible = true;
            title.visible = true;
            kbd2.visible = false;
        }).loop();
        
        var baby = new enchant.AtlasSprite("Atlas", "s1-embryo-0.png");
        baby.x = 22;
        baby.y = 16;
        this.addChild(baby);
        var theme = new MusicPlayer();
        if(joesheart.runcount > 0){
            theme.play("assets/sound/theme", true);
        }
        joesheart.runcount++;
        var self = this;
        joesheart.trackScene("TitleScreen", "Start");
        var buttonListener = function(){
            theme.stop();
            joesheart.trackScene("TitleScreen", "End");
            joesheart.trackScene("Embryo", "Start");
            var song = new MusicPlayer().play("assets/sound/birth", true);
            var speed = 0.5;
            var beatBegin;
            var st = beatBegin = game.getElapsedTime();
            var idx = 0;
            var max = 3;
            var heartBeat;
            joesheart.age = 0;
            var enterframe = function(){
                var t = game.getElapsedTime();
                if(t - st > speed){
                    st = t;
                    idx = (idx + 1) % max;
                    baby.spriteId = "s1-embryo-" + idx + ".png";
                    speed += 0.1;
                }
                
                if(t - beatBegin > 10){
                    self.removeEventListener(enchant.Event.ENTER_FRAME, enterframe);
                    heartBeat.dispose();
                    song.stop();
                    joesheart.trackScene("Embryo", "Win");
                    game.replaceScene(new WalkScene());
                }
            };
            
            heartBeat = new HeartBeat(1.2, self, 3, function(avg){
                self.removeEventListener(enchant.Event.ENTER_FRAME, enterframe);
                if(avg < -0.8){
                    baby.spriteId = "s1-embryo-white.png";
                    beatBegin = game.getElapsedTime();
                } else {
                    speed = 1 -  ((avg + 1) * 0.5);
                    self.addEventListener(enchant.Event.ENTER_FRAME, enterframe);
                }
            });
            
            this.removeChild(kbd);
            this.removeChild(kbd2);
            this.removeChild(title);
            this.removeChild(heart);
            
            self.removeEventListener(enchant.ENV.TOUCH_ENABLED ? 
                    enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, buttonListener);
        };
        
        this.tl.delay(5).then(function(){
            self.addEventListener(enchant.ENV.TOUCH_ENABLED ? 
                    enchant.Event.TOUCH_START : enchant.Event.A_BUTTON_DOWN, buttonListener);
        });
    }    
});