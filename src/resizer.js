;(function($) {
    $(function(){
        $(window).resize(function(e){
            var game = $("#enchant-stage");
            var wrapper = game.parent();
            // reset width/height
            game.css({ width: "auto", height: "auto"});
            var wi = wrapper.width();
            var he = wrapper.height();
            var ratio = game.data("aspect");
            if(he / wi < ratio){
                game.width(he / ratio);
                game.height(he);
                game.css("marginLeft", (wi - he / ratio) * 0.5);
                game.css("marginTop", 0);
            } else {
                game.width(wi);
                game.height(wi * ratio);
                game.css("marginTop", (he - wi * ratio) * 0.5);
                game.css("marginLeft", 0);
            }
            game.find("div").css("transform", "scale(1) !important");
        }).trigger("resize");
        
        $(document)
            .focus(function() { 
                $("#focusButton").hide();
            })
            .blur(function() { 
                $("#focusButton").show();
            });
        
        $("#focusButton").click(function(e){
            $(document).focus();
            e.preventDefault(); 
        });
    });
})(jQuery);