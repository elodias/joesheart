enchant();

window.onload = function() {
    var game = new Core(160, 90);
    game.fps = 60;
    game.loadingScene = new LoadScene();
    game.preload(joesheart.assets);
    
    game.addEventListener(enchant.Event.LOAD, function() {
        if(!enchant.ENV.TOUCH_ENABLED){
            // bind space to "A" button
            game.keybind(32, "a");
        }
        
        // create texture atlas
        enchant.TextureAtlas.createTextureAtlas("Atlas",
                game.assets["assets/atlas/Atlas.json"],
                game.assets["assets/atlas/Atlas.png"]);
        
        // determine whether or not to use autoload
        var useAutoLoad = (
            joesheart.autoLoadScene && 
            joesheart.debug && 
            typeof window[joesheart.autoLoadScene] == "function");
        
        // create the scene to load
        var scene = useAutoLoad ? 
                eval("new " + joesheart.autoLoadScene + "()") : new StartScene();
        
        
        game.pushScene(scene);
    });
    
    game.start();
    window.scrollTo(0, 0);
};