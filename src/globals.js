var joesheart = {
	// Hellow!
    // assets, globally assets should be added here, scene-only
    // assets should be added inside the scene JS file
    assets: ["assets/atlas/Atlas.png", "assets/atlas/Atlas.json"],
    
    // debug flag
    debug: true,
    
    // name of scene to autoload
    autoLoadScene: null,
    
    // joes age
    age: 0,
    
    runcount: 0,
    
    // whether or not to use ogg instead of mp3
    useOgg: navigator.userAgent.toLowerCase().match(/(firefox|opera)/) != null,
    
    loadJS: function(filename){
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", filename);
        document.getElementsByTagName("head")[0].appendChild(fileref);
    },
    
    loadScene: function(name){
        if(typeof name == "undefined"){
            return;
        }
        joesheart.autoLoadScene = name;
        joesheart.loadJS("src/scenes/" + name + ".js");
    },
    trackTime: {},
    trackScene: function(name, event){
        if ( typeof window._gaq !== 'undefined' ) {
            if(event == "Start"){
                joesheart.trackTime[name] = new Date().getTime();
                _gaq.push(['_trackEvent', 'Scenes', event, name]);
            } else {
                var t = (name in joesheart.trackTime) ? new Date().getTime() - joesheart.trackTime[name] : 0;
                _gaq.push(['_trackEvent', 'Scenes', event, name, t]);
            }
        }
    }
};


var query = window.location.search.replace( "?", "" );
if(query && (query != "") && joesheart.debug){
    joesheart.loadScene(query);
}
